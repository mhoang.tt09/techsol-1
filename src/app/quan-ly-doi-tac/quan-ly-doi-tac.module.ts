import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { QuanLyDoiTacComponent } from './quan-ly-doi-tac/quan-ly-doi-tac.component';
import { TaoMoiDoiTacComponent } from './tao-moi-doi-tac/tao-moi-doi-tac.component';

import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [QuanLyDoiTacComponent, TaoMoiDoiTacComponent],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  exports: [
    QuanLyDoiTacComponent,
    TaoMoiDoiTacComponent
  ]
})
export class QuanLyDoiTacModule { }
