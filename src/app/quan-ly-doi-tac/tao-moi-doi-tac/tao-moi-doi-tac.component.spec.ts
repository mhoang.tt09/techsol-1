import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaoMoiDoiTacComponent } from './tao-moi-doi-tac.component';

describe('TaoMoiDoiTacComponent', () => {
  let component: TaoMoiDoiTacComponent;
  let fixture: ComponentFixture<TaoMoiDoiTacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaoMoiDoiTacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaoMoiDoiTacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
