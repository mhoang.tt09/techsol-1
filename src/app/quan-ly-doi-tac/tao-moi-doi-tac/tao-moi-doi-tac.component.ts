import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '.app-tao-moi-doi-tac',
  templateUrl: './tao-moi-doi-tac.component.html',
  styleUrls: ['./tao-moi-doi-tac.component.scss']
})

export class TaoMoiDoiTacComponent implements OnInit {
  
  @Input('active_item')
  selected_item = {
    id: '',
    name: '',
    addr: '',
    tax: '',
    sale_channel: '',
    partner_type: '',
    active: true,
    created_by: '',
    created_at: {},
    updated_by: '',
    updated_at: {},
    email: '',
    phone: '',
    street: '',
    country: '',
    city: '',
    district: '',
    domain: '',
    port: '',
    footer: '',
  };
  
  PARTNERS: string[] = [
    'RE_Bán lẻ', 'WH_Bán sỉ', 'BA_Ngân hàng', 'IN_Bảo hiểm'
  ];
  DISTRICTS: string[] = [
    'Quận 1', 'Quận 2', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 6', 'Quận 7', 'Quận 8', 'Quận 9', 'Quận 10', 'Quận 11', 'Quận 12', 'Quận Bình Thạnh', 'Quận Bình Tân', 'Quận Gò Vấp', 'Quận Phú Nhuận', 'Quận Tân Bình', 'Quận Tân Phú', 'Quận Thủ Đức', 'Huyện Bình Chánh', 'Huyện Cần Giờ', 'Huyện Hooc Môn'
  ];
  
  
  constructor() { }
  
  ngOnInit() {
  }
  
  
}
