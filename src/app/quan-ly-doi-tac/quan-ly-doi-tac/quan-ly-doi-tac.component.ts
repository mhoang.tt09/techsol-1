import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router, ParamMap, NavigationEnd } from '@angular/router';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';

export interface UserData {
  id: string;
  name: string;
  addr: string;
  tax: string;
  partner_type: string;
  active: boolean;
  created_by: string;
  created_at: object;
  updated_by: string;
  updated_at: object;
  email: string;
  phone: string;
  street: string;
  country: string;
  city: string;
  district: string;
  domain: string;
  port: string;
  footer: string;
}

/** Constants used to fill up our data base. */
const IDS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
const STREETS: string[] = [
  'Quận 1', 'Quận 2', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 6', 'Quận 7', 'Quận 8', 'Quận 9', 'Quận 10', 'Quận 11', 'Quận 12', 'Quận Bình Thạnh', 'Huyện Bình Chánh', 'Quận Bình Tân', 'Quận Gò Vấp', 'Quận Phú Nhuận', 'Quận Tân Bình', 'Quận Tân Phú', 'Quận Thủ Đức', 'Huyện Cần Giờ', 'Huyện Hooc Môn'
];
const PARTNERS: string[] = [
  'RE_Bán lẻ', 'WH_Bán sỉ', 'BA_Ngân hàng', 'IN_Bảo hiểm'
];
const ACTIVES: boolean[] = [
  true, false
];
const CREATORS: string[] = [
  'Admin', 'Superuser'
];

/**
* @title Data table with sorting, pagination, and filtering.
*/

@Component({
  selector: '.app-quan-ly-doi-tac',
  templateUrl: './quan-ly-doi-tac.component.html',
  styleUrls: ['./quan-ly-doi-tac.component.scss']
})
export class QuanLyDoiTacComponent implements OnInit {
  
  public BH;
  public addForm: boolean = false;
  public detail: boolean = false;
  public detail_id: string;
  public active_item;
  
  displayedColumns: string[] = ['select', 'id', 'name', 'addr', 'tax', 'partner_type', 'active', 'created_by', 'created_at', 'updated_by', 'updated_at', 'action'];
  dataSource: MatTableDataSource<UserData>;
  selection = new SelectionModel<UserData>(true, []);
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(private route: ActivatedRoute, private router: Router) { 
    // Create 20 users
    const users = Array.from({length: 20}, (_, k) => createNewUser(k + 1));
    
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }
  
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
    this.route.paramMap.subscribe((params: ParamMap) => {
      let href = params.get('href');
      this.BH = href;
    });
  }
  
  reLoad(){
    const users2 = Array.from({length: 20}, (_, k) => createNewUser(k + 1));
    this.dataSource = new MatTableDataSource(users2);
  }
  
  public addDT(){
    this.addForm = !this.addForm;
    this.detail = false;
    this.detail_id = '';
  };
  public showDetail(row){
    this.detail = true;
    this.addForm = false;
    this.detail_id = row.id;
    this.active_item = row;
  }
  public hideDetail(){
    this.detail = false;
    this.detail_id = '';
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
  
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UserData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  
}

function randomDate(start, end, id) {
  return new Date(start.getTime() + (id * (end.getTime() - start.getTime())) );
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' + NAMES[Math.round(Math.random() * (NAMES.length - 1))];
  const str = Math.floor(Math.random() * 100).toString()+' '+NAMES[Math.round(Math.random() * (NAMES.length - 1))]+', Phường '+NAMES[Math.round(Math.random() * (NAMES.length - 1))];
  const dist = STREETS[Math.round(Math.random() * (STREETS.length - 1))];

  return {
    id: Math.round(Math.random() * 100).toString()+IDS[Math.round(Math.random() * (IDS.length - 1))],
    name: name,
    addr: str+', '+dist+', HCM, VN',
    tax: Math.floor(Math.random() * (10000 - 1000) + 1000).toString()+'-'+Math.floor(Math.random() * (1000 - 100) + 100).toString()+'-'+Math.floor(Math.random() * (1000 - 100) + 100).toString(),
    partner_type: PARTNERS[Math.round(Math.random() * (PARTNERS.length - 1))],
    active: ACTIVES[Math.round(Math.random() * (ACTIVES.length - 1))],
    created_by: CREATORS[Math.round(Math.random() * (CREATORS.length - 1))],
    created_at: randomDate(new Date(2019,0,1), new Date(2018,0,1), id/50),
    updated_by: CREATORS[Math.round(Math.random() * (CREATORS.length - 1))],
    updated_at: randomDate(new Date(), new Date(2019,0,1), Math.random()),
    email: NAMES[Math.round(Math.random() * (NAMES.length - 1))]+'@gmail.com',
    phone: '0'+Math.floor(Math.random() * (1000 - 100) + 100).toString()+' '+Math.floor(Math.random() * (1000 - 100) + 100).toString()+' '+Math.floor(Math.random() * (1000 - 100) + 100).toString(),
    street: str,
    country: 'Việt Nam',
    city: 'Hồ Chí Minh',
    district: dist,
    domain: 'localhost',
    port: '4200',
    footer: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam cum fuga ab expedita cumque pariatur in voluptatum nulla laborum aperiam possimus quos sed at alias tenetur, dignissimos quas atque neque.'
  };
}
