import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanLyDoiTacComponent } from './quan-ly-doi-tac.component';

describe('QuanLyDoiTacComponent', () => {
  let component: QuanLyDoiTacComponent;
  let fixture: ComponentFixture<QuanLyDoiTacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanLyDoiTacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanLyDoiTacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
