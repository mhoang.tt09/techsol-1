import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { ActivatedRoute, Router, ParamMap, NavigationEnd } from '@angular/router';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';

export interface UserData {
  id: string;
  name: string;
  gender: string;
  phone: string;
  email: string;
  acc_id: string;
  acc_pw: string;
  created_at: object;
  cty: string;
  branch: string;
  title: string;
  avatar: string;
  active: boolean;
  role: string[];
}

/** Constants used to fill up our data base. */
const NAMES: string[] = [
  'Mạnh', 'Dũng', 'Tuấn', 'Phương', 'Dung', 'Hạnh', 'Bông', 'Rồi', 'Vui', 'Cười', 'Lây', 'Há', 'Đực', 'Tí', 'Cò',
  'Mai', 'Lan', 'Cúc', 'Hoa', 'Hương', 'Yến', 'Oanh', 'Bích', 'Ngọc', 'Trân', 
  'Cương', 'Cường', 'Hùng', 'Tráng', 'Thông', 'Minh', 'Trí', 'Tuệ', 'Quang', 'Sáng', 'Nhân', 'Trung', 'Tín', 'Lễ', 'Nghĩa', 'Công', 'Hiệp', 'Phúc'
];
const FAMILYNAMES: string[] = [
  'Nguyễn', 'Trần', 'Lê', 'Phạm', 'Hoàng', 'Phan', 'Vũ', 'Võ', 'Đặng', 'Bùi',
  'Đỗ', 'Hồ', 'Ngô', 'Dương', 'Lý'
];
const GENDERS: string[] = [
  'Nam', 'Nữ'
];
const TITLES: string[] = [
  'Admin', 'Trưởng nhóm', 'Quản lý', 'Nhân viên', 'Kiểm soát viên'
];
const ACTIVES: boolean[] = [
  true, false
];

/**
* @title Data table with sorting, pagination, and filtering.
*/

@Component({
  selector: '.app-danh-sach-nhan-vien',
  templateUrl: './danh-sach-nhan-vien.component.html',
  styleUrls: ['./danh-sach-nhan-vien.component.scss']
})
export class DanhSachNhanVienComponent implements OnInit {
  
  @Input() cty: string = '';
  @Input() chiNhanh: string = '';
  
  displayedColumns: string[] = ['id', 'name', 'acc_id', 'role', 'gender', 'phone', 'email', 'created_at', 'active', 'action'];
  dataSource: MatTableDataSource<UserData>;
  selection = new SelectionModel<UserData>(true, []);
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(private route: ActivatedRoute, private router: Router) { 
    // Create 20 users
    const users = Array.from({length: 20}, (_, k) => createNewUser(k + 1));
    
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }
  
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.filterPredicate = (data: UserData, filtersJson: string) => {
      const matchFilter = [];
      const filters = JSON.parse(filtersJson);
      filters.forEach(filter => {
        const val = data[filter.id] === null ? '' : data[filter.id];
        matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()) );
      });
      return matchFilter.every(Boolean);
    };
  }
  
  reLoad(){
    const users2 = Array.from({length: 20}, (_, k) => createNewUser(k + 1));
    this.dataSource = new MatTableDataSource(users2);
  }
  
  applyFilter(filterValue: string, column: string) {
    const tableFilters = [];
    // const filterValue = (event.target as HTMLInputElement).value;

    tableFilters.push({
      id: column,
      value: filterValue
    });

    this.dataSource.filter = JSON.stringify(tableFilters);
    
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
    this.selection.clear() :
    this.dataSource.data.forEach(row => this.selection.select(row));
  }
  
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UserData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  
}

function randomDate(start, end, id) {
  return new Date(start.getTime() + (id * (end.getTime() - start.getTime())) );
}

function xoa_dau(str) {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
  str = str.replace(/Đ/g, "D");
  return str;
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))];
  const familyname = FAMILYNAMES[Math.round(Math.random() * (FAMILYNAMES.length - 1))];
  const gender = GENDERS[Math.round(Math.random() * (GENDERS.length - 1))];
  const title = TITLES[Math.round(Math.random() * (TITLES.length - 1))];
  
  const nvid = Math.floor(Math.random() * (10000 - 1000) + 1000).toString();
  
  return {
    id: nvid,
    name: name + ' ' + familyname,
    gender: gender,
    phone: '0'+Math.floor(Math.random() * (1000 - 100) + 100).toString()+' '+Math.floor(Math.random() * (1000 - 100) + 100).toString()+' '+Math.floor(Math.random() * (1000 - 100) + 100).toString(),
    created_at: randomDate(new Date(2021,0,1), new Date(2018,0,1), id/50),
    email: xoa_dau(name+familyname).toLowerCase()+Math.floor(Math.random() * (100 - 90) + 90).toString()+'@techsol.com',
    cty: 'Techsol',
    acc_id: xoa_dau(name+familyname).toLowerCase()+nvid,
    acc_pw: Math.floor(Math.random() * (10000000 - 1000000) + 1000000).toString(),
    branch: 'HCMC',
    title: title,
    avatar: 'avatar1',
    active: ACTIVES[Math.round(Math.random() * (ACTIVES.length - 1))],
    role: ['Admin', 'Trưởng nhóm', 'Quản lý', 'Nhân viên', 'Kiểm soát viên']
  };
}