import { Component, OnInit, Directive } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import {MatAutocompleteTrigger} from '@angular/material';

import {MAT_CHECKBOX_CLICK_ACTION} from '@angular/material/checkbox';

import { HttpClient } from  '@angular/common/http';
import { FileUploadAdapter } from  './file-upload.adapter';

import { ActivatedRoute, ParamMap, Router } from '@angular/router';


@Directive({
  selector: '[clickActionNoop]',
  providers: [{provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'noop'}],
})
export class ClickActionNoop {
}

@Component({
  selector: '.app-chi-tiet-nhan-vien',
  templateUrl: './chi-tiet-nhan-vien.component.html',
  styleUrls: ['./chi-tiet-nhan-vien.component.scss']
})
export class ChiTietNhanVienComponent implements OnInit  {
  
  adapter = new FileUploadAdapter(this.http);
  
  public today = new Date();
  public hide = true;
  public password = '';
  public create = false;
  public sendMail = false;
  public new = false;
  
  public ctyInput = '';
  public chiNhanhInput = '';
  public chucVuInput = '';
  
  ctyControl = new FormControl();
  chinhanhControl = new FormControl();
  chucvuControl = new FormControl();
  ctys: string[] = ['TECHSOL', 'ACB', 'VCB', 'VPBANK', 'TPBANK', 'BAOVIET', 'GIC', 'BICO', '...'];
  chinhanhs: string[] = ['Quận 1', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 7', 'Quận 10'];
  chucvus: string[] = ['Nhân viên', 'Kiểm soát viên', 'Trưởng nhóm', 'Quản lý khu vực', 'Phó giám đốc', 'Giám đốc'];
  filteredCtys: Observable<string[]>;
  filteredChinhanhs: Observable<string[]>;
  filteredChucvus: Observable<string[]>;
  
  roles = ['Admin', 'Quản lý', 'Trưởng nhóm', 'Thành viên', 'Kiểm soát viên'];
  
  constructor(private  http: HttpClient, private route: ActivatedRoute, private router: Router) { 
  }
  
  
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      let cty = params['cty'];
      let cn = params['chi_nhanh'];
      this.ctyInput = cty;
      this.chiNhanhInput = cn;
    });
    
    this.filteredCtys = this.ctyControl.valueChanges.pipe( startWith(''), map(value => this._filter(value, this.ctys)) );
    
    this.filteredChinhanhs = this.chinhanhControl.valueChanges.pipe( startWith(''), map(value => this._filter(value, this.chinhanhs)) );
    this.filteredChucvus = this.chucvuControl.valueChanges.pipe( startWith(''), map(value => this._filter(value, this.chucvus)) );
    
    if (!this.ctyInput || this.ctyInput == ''){
      this.chinhanhControl.disable();
      this.chucvuControl.disable();
    }
    
    this.password != '' ? this.create=true : this.create=false ;
  }
  
  goBack(){
    this.router.navigateByUrl('/qlnv');
  }
  
  private _filter(value: string, array: string[]): string[] {
    const filterValue = value.toLowerCase();
    
    return array.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  
  clearAuto(input, control, auto, trigger: MatAutocompleteTrigger){
    if(control != this.ctyControl){
      input = '';
      auto.options.forEach((item) => {
        item.deselect();
      });
      control.reset('');
    }
    else{
      input = '';
      this.chiNhanhInput = '';
      this.chucVuInput = '';
      
      control.reset('');
    }
    setTimeout(() => {
      trigger.openPanel();
    }, 0);
  }
  
  toggleCtrl(value){
    if (value){
      this.chinhanhControl.enable();
      this.chucvuControl.enable();
    }
    else{
      this.chinhanhControl.disable();
      this.chucvuControl.disable();
    }
  }
  
  change(roleList, selectAll){
    let l = roleList.selectedOptions.selected.length;
    let s = this.roles.length;
    if (l <= 0){
      selectAll.checked = false;
      selectAll.indeterminate = false;
    } else{
      if(l < s){
        selectAll.indeterminate = true;
      } else {
        selectAll.checked = true;
        selectAll.indeterminate = false;
      }
    }
  }
}

