import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatAutocompleteTrigger} from '@angular/material';

@Component({
  selector: '.app-quan-ly-nhan-vien',
  templateUrl: './quan-ly-nhan-vien.component.html',
  styleUrls: ['./quan-ly-nhan-vien.component.scss']
})
export class QuanLyNhanVienComponent implements OnInit {
  
  public nvID;
  
  public ctyInput = 'TECHSOL';
  public chiNhanhInput = 'Quận 1';
  
  ctyControl = new FormControl();
  chinhanhControl = new FormControl();

  ctys: string[] = ['TECHSOL', 'ACB', 'VCB', 'VPBANK', 'TPBANK', 'BAOVIET', 'GIC', 'BICO', '...'];
  chinhanhs: string[] = ['Quận 1', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 7', 'Quận 10'];

  filteredCtys: Observable<string[]>;
  filteredChinhanhs: Observable<string[]>;
  
  constructor(private route: ActivatedRoute, private router: Router) { }
  
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let href = params.get('href');
      this.nvID = href;
    });
    
    this.filteredCtys = this.ctyControl.valueChanges.pipe( startWith(''), map(value => this._filter(value, this.ctys)) );
    this.filteredChinhanhs = this.chinhanhControl.valueChanges.pipe( startWith(''), map(value => this._filter(value, this.chinhanhs)) );
    if (!this.ctyInput || this.ctyInput == ''){
      this.chinhanhControl.disable();
    }
  }
  
  
  private _filter(value: string, array: string[]): string[] {
    const filterValue = value.toLowerCase();
    
    return array.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  clearAuto(input, control, auto, trigger: MatAutocompleteTrigger){
    if(control != this.ctyControl){
      input = '';
      auto.options.forEach((item) => {
        item.deselect();
      });
      control.reset('');
    }
    else{
      input = '';
      this.chiNhanhInput = '';
      
      control.reset('');
    }
    // setTimeout(() => {
    //   trigger.openPanel();
    // }, 0);
    this.trigger(trigger);
  }

  trigger(trigger: MatAutocompleteTrigger){
    setTimeout(() => {
      trigger.openPanel();
    }, 0);
  }
  
  toggleCtrl(value){
    if (value){
      this.chinhanhControl.enable();
    }
    else{
      this.chinhanhControl.disable();
    }
  }
  
}
