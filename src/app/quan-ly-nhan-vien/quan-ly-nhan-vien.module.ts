import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DanhSachNhanVienComponent } from './danh-sach-nhan-vien/danh-sach-nhan-vien.component';
import { QuanLyNhanVienComponent } from './quan-ly-nhan-vien/quan-ly-nhan-vien.component';
import { CapQuyenNhanVienComponent } from './cap-quyen-nhan-vien/cap-quyen-nhan-vien.component';

import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { ChiTietNhanVienComponent, ClickActionNoop } from './chi-tiet-nhan-vien/chi-tiet-nhan-vien.component';



@NgModule({
  declarations: [
    DanhSachNhanVienComponent, 
    QuanLyNhanVienComponent, 
    CapQuyenNhanVienComponent, 
    ChiTietNhanVienComponent,
    ClickActionNoop
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    DanhSachNhanVienComponent,
    QuanLyNhanVienComponent,
    CapQuyenNhanVienComponent,
    ClickActionNoop
  ],

})
export class QuanLyNhanVienModule { }
