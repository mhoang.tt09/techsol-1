import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuanLyHoSoComponent } from './quan-ly-ho-so/quan-ly-ho-so/quan-ly-ho-so.component';
import { DanhSachHoSoComponent } from './quan-ly-ho-so/danh-sach-ho-so/danh-sach-ho-so.component';
import { ChiTietHoSoComponent } from './quan-ly-ho-so/chi-tiet-ho-so/chi-tiet-ho-so.component';

import { QuanLyNhanVienComponent } from './quan-ly-nhan-vien/quan-ly-nhan-vien/quan-ly-nhan-vien.component';
// import { DanhSachNhanVienComponent } from './quan-ly-nhan-vien/danh-sach-nhan-vien/danh-sach-nhan-vien.component';
// import { CapQuyenNhanVienComponent } from './quan-ly-nhan-vien/cap-quyen-nhan-vien/cap-quyen-nhan-vien.component';
import { ChiTietNhanVienComponent } from './quan-ly-nhan-vien/chi-tiet-nhan-vien/chi-tiet-nhan-vien.component';

import { QuanLyDoiTacComponent } from './quan-ly-doi-tac/quan-ly-doi-tac/quan-ly-doi-tac.component';
import { TaoMoiDoiTacComponent } from './quan-ly-doi-tac/tao-moi-doi-tac/tao-moi-doi-tac.component';


const routes: Routes = [
  {path: '',  redirectTo: '/qlhs/bh-dl', pathMatch: 'full'},
  
  {path: 'qlhs', component: QuanLyHoSoComponent, children:[
    {path: '', redirectTo: '/qlhs/bh-dl', pathMatch: 'full'}
  ]},
  {path: 'qlhs/:href', component: DanhSachHoSoComponent},
  {path: 'qlhs/:href/tao-moi', component: ChiTietHoSoComponent},
  
  {path: 'qlnv', component: QuanLyNhanVienComponent},
  {path: 'qlnv/tao-moi', component: ChiTietNhanVienComponent},
  
  {path: 'qldt', component: QuanLyDoiTacComponent},
  {path: 'qldt/tao-moi', component: TaoMoiDoiTacComponent},
  
  {path: "**", component: QuanLyHoSoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
