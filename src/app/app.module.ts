import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';

import { LayoutsModule } from './layouts/layouts.module';
import { QuanLyHoSoModule } from './quan-ly-ho-so/quan-ly-ho-so.module';
import { QuanLyDoiTacModule } from './quan-ly-doi-tac/quan-ly-doi-tac.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    

    BrowserAnimationsModule,
    MaterialModule,

    LayoutsModule,
    QuanLyHoSoModule,
    QuanLyDoiTacModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
