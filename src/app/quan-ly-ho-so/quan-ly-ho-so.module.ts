import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DanhSachHoSoComponent } from './danh-sach-ho-so/danh-sach-ho-so.component';
import { ChiTietHoSoComponent } from './chi-tiet-ho-so/chi-tiet-ho-so.component';
import { MaterialModule } from '../material/material.module';
import { QuanLyHoSoComponent } from './quan-ly-ho-so/quan-ly-ho-so.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [DanhSachHoSoComponent, ChiTietHoSoComponent, QuanLyHoSoComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    DanhSachHoSoComponent,
    ChiTietHoSoComponent,
    QuanLyHoSoComponent
  ]
})
export class QuanLyHoSoModule { }
