import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormControl } from '@angular/forms';


@Component({
  selector: '.app-chi-tiet-ho-so',
  templateUrl: './chi-tiet-ho-so.component.html',
  styleUrls: ['./chi-tiet-ho-so.component.scss'],
})
export class ChiTietHoSoComponent implements OnInit {
  
  public flag:boolean = true;
  public tooltip = "Thu gọn";
  
  public ghiChu = "A. Tài khoản chuyên thu: 3939 7968 001 <br> B. Cú pháp: [ID]-[Họ tên KH]-[Miscode nhân viên]-[Mã đơn vị,Phí bảo hiểm]-2 <br> Lưu ý: Cú pháp viết liền không khoảng trắng";
  
  public bhTxt;
  public menuList: [
    {href:  'bh-dl',      name: 'BH Du lịch'},
    {href:  'bh-xcg',     name: 'BH Xe cơ giới'},
    {href:  'bh-xgm',     name: 'BH Xe gắn máy'},
    {href:  'bh-sktd',    name: 'BH Sức khỏe toàn diện'},
    {href:  'bh-ut',      name: 'BH Ung thư'},
    {href:  'bh-nt',      name: 'BH Nhân thọ'},
    {href:  'bh-tc',      name: 'BH Tài chính'},
    {href:  'bh-vtc',     name: 'BH Vay tín chấp'},
    {href:  'bh-dn',      name: 'BH Doanh nghiệp'}
  ];
  
  constructor(private route: ActivatedRoute,private router:Router) { }
  
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let href = params.get('href');
      switch (href){
        case 'bh-dl':     this.bhTxt = 'BH Du lịch'
        break;
        case  'bh-xcg':   this.bhTxt = 'BH Xe cơ giới'
        break;
        case  'bh-xgm':   this.bhTxt = 'BH Xe gắn máy'
        break;
        case  'bh-sktd':  this.bhTxt = 'BH Sức khỏe toàn diện'
        break;
        case  'bh-ut':    this.bhTxt = 'BH Ung thư'
        break;
        case  'bh-nt':    this.bhTxt = 'BH Nhân thọ'
        break;
        case  'bh-tc':    this.bhTxt = 'BH Tài chính'
        break;
        case  'bh-vtc':   this.bhTxt = 'BH Vay tín chấp'
        break;
        case  'bh-dn':    this.bhTxt = 'BH Doanh nghiệp'
        break;
      }
    });
  };
  
  onSelect(href){
    this.router.navigate(href, {relativeTo: this.route});
  }
  
  changeText(){
    this.flag = !this.flag;
    if (this.flag) {
      this.tooltip = "Thu gọn";
    }
    else {
      this.tooltip = "Mở rộng";
    }
  }
  
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  
  today = new Date();
  
  
}
