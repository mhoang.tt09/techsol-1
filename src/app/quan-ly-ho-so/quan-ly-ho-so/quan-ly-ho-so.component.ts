import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';


@Component({
  selector: 'app-quan-ly-ho-so',
  templateUrl: './quan-ly-ho-so.component.html',
  styleUrls: ['./quan-ly-ho-so.component.scss']
})
export class QuanLyHoSoComponent implements OnInit {
  
  public BH;
  
  constructor(private route: ActivatedRoute, private router: Router) { }
  
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let href = params.get('href');
      this.BH = href;
    });
  }
  
}
