import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: '.app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  
  public Menus = [
    {
      menuName: 'Quản Lý Hồ Sơ',
      icon: 'folder',
      menuList: [
        {href:  'bh-dl',      name: 'BH Du lịch'},
        {href:  'bh-xcg',     name: 'BH Xe cơ giới'},
        {href:  'bh-xgm',     name: 'BH Xe gắn máy'},
        {href:  'bh-sktd',    name: 'BH Sức khỏe toàn diện'},
        {href:  'bh-ut',      name: 'BH Ung thư'},
        {href:  'bh-nt',      name: 'BH Nhân thọ'},
        {href:  'bh-tc',      name: 'BH Tài chính'},
        {href:  'bh-vtc',     name: 'BH Vay tín chấp'},
        {href:  'bh-dn',      name: 'BH Doanh nghiệp'}
      ],
      menuUrl: 'qlhs'
    },
    
    {
      menuName: 'Quản Lý Nhân Viên',
      icon: 'assignment_ind',
      menuList : [
        
      ],
      menuUrl: 'qlnv'
    },

    {
      menuName: 'Quản Lý Đối Tác',
      icon: 'business_center',
      menuList : [
        
      ],
      menuUrl: 'qldt'
    }
  ];
  
  public BH;
  
  constructor(private route: ActivatedRoute,private router:Router) { }
  
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let href = params.get('href');
      this.BH = href;
    });
  }
  
  onSelect(href){
    this.router.navigate(href, {relativeTo: this.route});
  }
  
}
